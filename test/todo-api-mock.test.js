import axios from "axios";
import todoApi from "../todo-api";
import taskService from "../task-service";

axios.defaults.adapter = require("axios/lib/adapters/http");
axios.defaults.baseURL = "http://localhost:3001";

jest.mock("../task-service");

const testData = [
    { id: 1, title: "Les leksjon", done: 1 },
    { id: 2, title: "Møt opp på forelesning", done: 0 },
    { id: 3, title: "Gjør øving", done: 0 },
];

let webServer;
beforeAll((done) => (webServer = todoApi.listen(3001, () => done())));

afterAll((done) => webServer.close(() => done()));

describe("Fetch tasks (GET)", () => {
    test("Fetch all tasks (200 OK)", async () => {
        taskService.getAll = jest.fn(() => Promise.resolve(testData));

        const response = await axios.get("/api/v1/tasks");
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testData);
    });

    test("Fetch task (200 OK)", async () => {
        taskService.get = jest.fn(() => Promise.resolve(testData[0]));

        const response = await axios.get("/api/v1/tasks/1");
        expect(response.status).toEqual(200);
        expect(response.data).toEqual(testData[0]);
    });

    test("Fetch all tasks (500 Internal Server Error)", async () => {
        taskService.getAll = jest.fn(() => Promise.reject());

        try {
            const response = await axios.get("/api/v1/tasks");
        } catch (err) {
            expect(err.response.status).toEqual(500);
        }
    });

    test("Fetch task (404 Not Found)", async () => {
        taskService.get = jest.fn(() => Promise.resolve([]));

        try {
            const response = await axios.get("/api/v1/tasks/10");
        } catch (err) {
            expect(err.response.status).toEqual(404);
        }
    });

    test("Fetch task (500 Internal Server error)", async () => {
        taskService.get = jest.fn(() => Promise.reject());

        try {
            const response = await axios.get("/api/v1/tasks/1");
        } catch (err) {
            expect(err.response.status).toEqual(500);
        }
    });
});

describe("Create new task (POST)", () => {
    test("Create new task (201 Created)", async () => {
        const newTask = { id: 4, title: "Få øving godkjent", done: 0 };
        taskService.create = jest.fn(() => Promise.resolve(newTask));

        const response = await axios.post("/api/v1/tasks", newTask);
        expect(response.status).toEqual(201);
        expect(response.headers.location).toEqual("tasks/4");
    });

    test("Create new task (400 Bad Request)", async () => {
        const newTask = { id: 4, title: "Få øving godkjent" };

        try {
            const response = await axios.post("/api/v1/tasks", newTask);
        } catch (err) {
            expect(err.response.status).toEqual(400);
        }
    });

    test("Create new task (500 Internal Server error)", async () => {
        taskService.create = jest.fn(() => Promise.reject());

        const newTask = { id: 4, title: "Få øving godkjent", done: 0 };

        try {
            const response = await axios.post("/api/v1/tasks", newTask);
        } catch (err) {
            expect(err.response.status).toEqual(500);
        }
    });
});

describe("Delete task (DELETE)", () => {
    test("Delete task (200 OK)", async () => {
        taskService.delete = jest.fn(() => Promise.resolve());

        const response = await axios.delete("/api/v1/tasks/1");
        expect(response.status).toEqual(200);
    });
});
